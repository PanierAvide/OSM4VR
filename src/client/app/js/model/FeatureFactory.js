/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cartesian3 from 'cesium/Source/Core/Cartesian3';
import Color from 'cesium/Source/Core/Color';

/**
 * The feature factory takes in input data from providers (GeoJSON), and generates appropriate 3D models.
 */
class FeatureFactory {
	/**
	 * Converts a given GeoJSON feature into a CesiumJS 3D entity
	 * @param {Object} feature The {@link http://geojson.org/geojson-spec.html#feature-objects|GeoJSON feature}
	 * @return {Object} The {@link https://cesiumjs.org/Cesium/Build/Documentation/Entity.html|Cesium entity}
	 */
	geojsonToEntity(feature) {
		const result = {
			id: feature.id
		};
		
		/*
		 * Geometry construction
		 */
		
		const coords = feature.geometry.coordinates;
		
		switch(feature.geometry.type) {
			case "Point":
				result.position = Cartesian3.fromDegrees(coords[0], coords[1], 0);
				break;
			
			case "LineString":
				result.polyline = {
					positions: Cartesian3.fromDegreesArray([].concat.apply([], coords))
				};
				break;
			
			case "Polygon":
				const coords2 = coords.map((x) => { x.pop(); return x; });
				result.polygon = {
					hierarchy: {
						positions: Cartesian3.fromDegreesArray([].concat.apply([], coords2[0]))
					}
				};
				
				//Holes
				if(coords.length > 1) {
					result.polygon.hierarchy.holes = [];
					
					for(let i=1; i < coords2.length; i++) {
						result.polygon.hierarchy.holes.push({
							positions: Cartesian3.fromDegreesArray([].concat.apply([], coords2[i]))
						});
					}
				}
				
				break;
		}
		
		
		/*
		 * Colors and shape
		 */
		
		switch(feature.geometry.type) {
			case "Point":
				result.ellipsoid = {
					radii: new Cartesian3(1, 1, 1),
					material: Color.BLUE
				};
				break;
			
			case "LineString":
				result.polyline.width = 5;
				result.polyline.material = Color.RED;
				break;
			
			case "Polygon":
				result.polygon.material = Color.GREEN;
				break;
		}

		return result;
	}
}

module.exports = FeatureFactory;
