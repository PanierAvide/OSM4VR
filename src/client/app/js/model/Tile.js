/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

const LatLng = require("./LatLng");
const LatLngBounds = require("./LatLngBounds");

/**
 * A tile is an extent covering a given surface of the Earth.
 * It follows rules specified here: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
 */
class Tile {
	/**
	 * Constructor using tile index
	 * @param {int} x The x value (longitude)
	 * @param {int} y The y value (latitude)
	 * @param {int} z The z value (zoom)
	 */
	constructor(x, y, z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Creates a tile from coordinates and zoom
	 * @param {LatLng} latlng The coordinates (WGS84)
	 * @param {int} zoom The zoom
	 * @return {Tile} The created tile
	 */
	static fromLatLngZoom(latlng, zoom) {
		return new Tile(
			Math.floor( (latlng.lng + 180) / 360 * Math.pow(2,zoom) ),
			Math.floor( (1 - Math.log(Math.tan(latlng.lat * Math.PI / 180) + 1 / Math.cos(latlng.lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom) ),
			zoom
		);
	}
	
	/**
	 * Get the tile ID, in z/x/y format
	 * @return {string} The tile ID
	 */
	getId() {
		return this.z + "/" + this.x + "/" + this.y;
	}
	
	/**
	 * Get the bounding box of the tile
	 * @return {LatLngBounds} The tile bounds
	 */
	getBounds() {
		return new LatLngBounds(
			new LatLng(this.tileY2lat(this.y + 1, this.z), this.tileX2lon(this.x, this.z)),
			new LatLng(this.tileY2lat(this.y, this.z), this.tileX2lon(this.x + 1, this.z))
		);
	}
	
	/**
	 * Converts tile Y value into latitude
	 * @private
	 * @param {int} y The Y value
	 * @param {int} z The Z value
	 * @return {float} The latitude (WGS84)
	 */
	tileY2lat(y, z) {
		const n = Math.PI - 2 * Math.PI * y / Math.pow(2,z);
		return (180 / Math.PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))));
	}
	
	/**
	 * Converts tile X value into longitude
	 * @private
	 * @param {int} x The X value
	 * @param {int} z The Z value
	 * @return {float} The longitude (WGS84)
	 */
	tileX2lon(x, z) {
		return (x / Math.pow(2, z) * 360 - 180);
	}
}

module.exports = Tile;
