/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import { connect } from 'react-redux';
import { NotifTypes } from '../ctrl/actions';
import Notification from './Notification.jsx';

const mapStateToProps = (state, ownProps) => {
	if(state.notification.message !== undefined) {
		let icon = null;
		
		switch(state.notification.type) {
			case NotifTypes.INFO:
				icon = "info-sign";
				break;
			case NotifTypes.ALERT:
				icon = "warning-sign";
				break;
			case NotifTypes.ERROR:
				icon = "exclamation-sign";
				break;
			case NotifTypes.SUCCESS:
				icon = "ok-sign";
				break;
		}
		
		return {
			show: true,
			message: state.notification.message,
			type: state.notification.type,
			icon: icon
		};
	}
	else {
		return { show: false };
	}
};

/**
 * Container component around {@link Notification} presentational component.
 */
const NotificationContainer = connect(
	mapStateToProps
)(Notification);

module.exports = NotificationContainer;
