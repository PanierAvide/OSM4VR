/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Alert from 'react-bootstrap/lib/Alert';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Overlay from 'react-bootstrap/lib/Overlay';

/**
 * Notification overlay component.
 * It allows to display short messages to user, like downloading status or errors.
 * 
 * @property {boolean} show Show or hide this component
 * @property {string} message The text of the notification
 * @property {string} type The kind of message (info, warning, alert, success)
 * @property {string} [icon] The icon at the start of message ({@link http://getbootstrap.com/components/#glyphicons|list}, use without "glyphicon-" prefix)
 * 
 * @example
 * <Notification
 * 	show=true
 * 	message="Some message to display"
 * 	type="info"
 * 	icon="alert-sign"
 * />
 */
class Notification extends React.Component {
	render() {
		let glyph = null;
		
		if(this.props.icon) {
			glyph = <Glyphicon glyph={this.props.icon} />;
		}
		
		return (
			<Overlay
				show={this.props.show}
				>
				<Alert
					id="osm4vr-notification"
					bsStyle={this.props.type}
					style={{
						boxShadow: '0 5px 10px rgba(0, 0, 0, 0.2)'
					}}
				>
					{glyph} {this.props.message}
				</Alert>
			</Overlay>
		);
	}
}

module.exports = Notification;
