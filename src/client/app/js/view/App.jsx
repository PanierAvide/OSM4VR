/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import '../../css/app.css';
import Header from './Header.jsx'
import NotificationContainer from './NotificationContainer.jsx'
import Map from './Map.jsx'
import 'cesium/Source/Widgets/widgets.css';
import buildModuleUrl from 'cesium/Source/Core/buildModuleUrl';
buildModuleUrl.setBaseUrl('./public/cesium/');

/**
 * Main application component.
 * It creates the DOM tree for the whole page (header, map...).
 */
class App extends React.Component {
	render() {
		return (
			<div>
				<Header />
				<NotificationContainer />
				<Map />
			</div>
		);
	}
}

module.exports = App;
