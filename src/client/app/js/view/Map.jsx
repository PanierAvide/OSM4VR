/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import CesiumHash from '../ctrl/CesiumHash';
import CMath from 'cesium/Source/Core/Math';
import Credit from 'cesium/Source/Core/Credit';
import createOpenStreetMapImageryProvider from 'cesium/Source/Scene/createOpenStreetMapImageryProvider';
import Ellipsoid from 'cesium/Source/Core/Ellipsoid';
import FeatureFactory from '../model/FeatureFactory';
import LatLng from '../model/LatLng';
import LatLngBounds from '../model/LatLngBounds';
import React from 'react';
import TileManager from '../ctrl/TileManager';
import Viewer from 'cesium/Source/Widgets/Viewer/Viewer';
import { showNotification, hideNotification, NotifTypes } from '../ctrl/actions';

/**
 * Map component.
 * It uses CesiumJS for 3D Earth rendering, and calls {@link TileManager} for retrieving needed data.
 */
class Map extends React.Component {
	componentDidMount() {
		this._tileManager = new TileManager();
		this._featureFactory = new FeatureFactory();
		this._visibleTiles = [];
		
		//Init cesium viewer
		this._viewer = new Viewer(this.cesiumContainer, {
			animation : false,
			baseLayerPicker : false,
			fullscreenButton : false,
			geocoder : false,
			homeButton : true,
			infoBox : false,
			sceneModePicker : false,
			selectionIndicator : true,
			timeline : false,
			navigationHelpButton : true,
			scene3DOnly : true,
			imageryProvider: createOpenStreetMapImageryProvider({
				maximumLevel: 19,
				credit: new Credit('© OpenStreetMap contributors', null, 'https://openstreetmap.org/copyright')
			})
		});
		
		const cHash = new CesiumHash(this._viewer);
		
		//Check periodically the camera bbox
		let firstLoad = true;
		this._lastBBox = null;
		this._lastDownloadBBox = null;
		
		this._timerBBox = setInterval(() => {
			const bbox = this.getBBox();
			
			//Zoom in message
			if(
				this.context.store.getState().notification.message === undefined
				&& (
					bbox === null
					|| firstLoad
					|| this._lastBBox === null
					|| bbox.getSouthWest().distanceTo(bbox.getNorthEast()) >= 500
				)
			) {
				this.context.store.dispatch(showNotification("Please zoom in to see 3D data", NotifTypes.INFO));
			}
			
			//Fetch data only if BBox is "reasonable"
			if(
				bbox !== null
				&& (firstLoad || (
					this._lastBBox !== null
					&& !bbox.equals(this._lastBBox)
					&& (this._lastDownloadBBox === null || !this._lastDownloadBBox.contains(bbox))
				))
				&& bbox.getSouthWest().distanceTo(bbox.getNorthEast()) < 500
			) {
				firstLoad = false;
				
				//Wait if user is still moving around
				if(this._timerDataDownload !== undefined) {
					clearTimeout(this._timerDataDownload);
				}
				
				if(this.context.store.getState().notification.message === "Please zoom in to see 3D data") {
					this.context.store.dispatch(hideNotification());
				}
				
				this._timerDataDownload = setTimeout(() => {
					this._viewer.entities.suspendEvents();
					
					//Remove too far tiles
					for(const tileData of Object.entries(this._tileManager.dropTiles(bbox.getCenter(), 15))) {
						if(this._visibleTiles.indexOf(tileData[0]) >= 0) {
							console.log("[Map] Dropping tile "+tileData[0]);
							
							for(const feature of tileData[1].data.features) {
								this._viewer.entities.removeById(feature.id);
							}
							
							//Remove tile from visible list
							this._visibleTiles.splice(this._visibleTiles.indexOf(tileData[0]), 1);
						}
					}
					
					this._viewer.entities.resumeEvents();
					
					console.log("[Map] Fetching BBox "+bbox.toBBoxString());
					this.context.store.dispatch(showNotification("Downloading 3D data", NotifTypes.INFO));
					
					this._tileManager
						.getTiles(bbox, 15)
						.then((tilesData) => {
							this._viewer.entities.suspendEvents();
							
							//Add new tiles
							for(const tileData of Object.entries(tilesData)) {
								if(this._visibleTiles.indexOf(tileData[0]) < 0) {
									for(const feature of tileData[1].data.features) {
										try {
											this._viewer.entities.add(this._featureFactory.geojsonToEntity(feature));
										}
										catch(e) {;}
									}
									this._visibleTiles.push(tileData[0]);
								}
								else {
									console.log("[Map] Skip tile "+tileData[0]);
								}
							}
							this.context.store.dispatch(hideNotification());
							console.log("[Map] Loaded data for BBox "+bbox.toBBoxString());
							
							this._viewer.entities.resumeEvents();
						});
					this._lastDownloadBBox = bbox;
				}, 1500);
			}
			
			this._lastBBox = bbox;
		}, 500);
	}
	
	render() {
		return (
			<div>
				<div id="osm4vr-map" ref={ element => this.cesiumContainer = element }/>
			</div>
		);
	}
	
	componentWillUnmount() {
		//Clear timers
		if(this._timerBBox) {
			clearInterval(this._timerBBox);
		}
		if(this._timerDataDownload) {
			clearTimeout(this._timerDataDownload);
		}
		
		//Clear Cesium
		delete this._viewer;
	}
	
	/**
	 * Get the camera current view bounding box
	 * @return {model.LatLngBounds} The bounding box of visible area (or null if camera pointing somewhere else)
	 */
	getBBox() {
		const rect = this._viewer.camera.computeViewRectangle(Ellipsoid.WGS84);
		let bbox = null;
		if(rect) {
			bbox = new LatLngBounds(
				new LatLng(
					CMath.toDegrees(rect.south).toFixed(6),
					CMath.toDegrees(rect.west).toFixed(6)
				),
				new LatLng(
					CMath.toDegrees(rect.north).toFixed(6),
					CMath.toDegrees(rect.east).toFixed(6)
				)
			);
		}
		return bbox;
	}
}

module.exports = Map;
