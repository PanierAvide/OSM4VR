/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import Navbar from 'react-bootstrap/lib/Navbar';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';

/**
 * Page header component.
 * It handles the header part of the page (brand, menus).
 */
class Header extends React.Component {
	render() {
		return (
			<Navbar fixedTop fluid>
				<Navbar.Header>
					<Navbar.Brand>
						<a href="#">OSM4VR</a>
					</Navbar.Brand>
					<Navbar.Toggle />
				</Navbar.Header>
				<Navbar.Collapse>
					<Nav pullRight>
						<NavItem eventKey={2} href="https://wiki.openstreetmap.org/wiki/OSM_go" target="_blank">Help</NavItem>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		);
	}
}

module.exports = Header;
