/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import LatLng from '../model/LatLng';
import LatLngBounds from '../model/LatLngBounds';
import Overpass from './provider/Overpass';
import Tile from '../model/Tile';

import CONFIG from '../../../config/config.json';
import POLYGON_FEATURES from '../../../config/polygon_features.json';

/**
 * Tile manager handles tiles data retrieval (using providers classes) and storage.
 */
class TileManager {
	/**
	 * Constructor
	 */
	constructor() {
		this._overpass = new Overpass(CONFIG.data_providers.overpass, {
			polygonFeatures: POLYGON_FEATURES
		});
		
		this._loadedTiles = {};
	}
	
	/**
	 * Loads (from cache or data provider) tiles covering the given bounding box
	 * @param {LatLngBounds} bbox The area to retrieve
	 * @param {int} zoom The zoom value
	 * @return {Promise} A promise that will give you tiles data when ready
	 */
	getTiles(bbox, zoom) {
		return new Promise((resolve, reject) => {
			const tiles = this.bbox2tiles(bbox, zoom);
			const missingTiles = [];
			
			//Find tiles not yet downloaded
			for(const tile of tiles) {
				if(this._loadedTiles[tile.getId()] === undefined) {
					missingTiles.push(tile);
				}
			}
			
			//Download missing tiles
			if(missingTiles.length > 0) {
				let tilesDone = 0;
				for(const mTile of missingTiles) {
					this._overpass.requestAll(
						mTile.getBounds(),
						(geojson) => {
							this._loadedTiles[mTile.getId()] = { tile: mTile, data: geojson };
							tilesDone++;
							
							if(tilesDone == missingTiles.length) {
								resolve(this._getTilesFromMemory(tiles));
							}
						},
						(error) => {
							console.log("[TileManager] Can't retrieve tile "+mTile.getId()+" from Overpass");
						}
					);
				}
			}
			//Send tiles from memory
			else {
				resolve(this._getTilesFromMemory(tiles));
			}
		});
	}
	
	/**
	 * Send tiles to given callback function
	 * @param {Tile[]} tiles The list of tiles to retrieve from memory
	 * @return {Object} The tiles data, as an array of { tile: Tile, data: GeoJSON } objects
	 * @private
	 */
	_getTilesFromMemory(tiles) {
		const tilesData = {};
		
		for(const tile of tiles) {
			tilesData[tile.getId()] = this._loadedTiles[tile.getId()];
		}
		
		return tilesData;
	}
	
	/**
	 * Drops tiles which are too far from given coordinates (according to zoom)
	 * @param {LatLng} latlng The current coordinates
	 * @param {int} zoom The current zoom
	 * @return {Object} The dropped tiles, as { tileId: { tile: Tile, data: GeoJSON } }
	 */
	dropTiles(latlng, zoom) {
		const userTile = Tile.fromLatLngZoom(latlng, zoom);
		const droppedTiles = {};
		
		for(const tileId in this._loadedTiles) {
			const currTile = this._loadedTiles[tileId];
			
			//If tile too far away, we drop it
			if(
				currTile !== undefined
				&& currTile.tile.z === zoom
				&& (
					Math.abs(currTile.tile.x - userTile.x) >= CONFIG.data_view.max_tiles_radius
					|| Math.abs(currTile.tile.y - userTile.y) >= CONFIG.data_view.max_tiles_radius
				)
			) {
				droppedTiles[tileId] = this._loadedTiles[tileId];
				this._loadedTiles[tileId] = undefined;
			}
		}
		
		return droppedTiles;
	}
	
	/**
	 * Get the list of tiles covering the given bounding box
	 * @param {LatLngBounds} bbox The area to retrieve
	 * @param {int} zoom The zoom value
	 * @return {Tile[]} The list of tiles
	 */
	bbox2tiles(bbox, zoom) {
		//BBox over east limit
		if(bbox.getEast() >= 180) {
			//Split into 2 bboxes (one each side of the limit)
			return this.bbox2tiles(
				new LatLngBounds(
					bbox.getSouthWest(),
					new LatLng(bbox.getNorth(), 179.999999999)
				),
				zoom
			).concat(this.bbox2tiles(
				new LatLngBounds(
					new LatLng(bbox.getSouth(), -180),
					new LatLng(bbox.getNorth(), bbox.getEast() - 360)
				),
				zoom
			));
		}
		//BBox over west limit
		else if(bbox.getWest() < -180) {
			//Split into 2 bboxes (one each side of the limit)
			return this.bbox2tiles(
				new LatLngBounds(
					new LatLng(bbox.getSouth(), bbox.getWest() + 360),
					new LatLng(bbox.getNorth(), 179.999999999)
				),
				zoom
			).concat(this.bbox2tiles(
				new LatLngBounds(
					new LatLng(bbox.getSouth(), -180),
					bbox.getNorthEast()
				),
				zoom
			));
		}
		//BBox in bounds
		else {
			const tileSW = Tile.fromLatLngZoom(bbox.getSouthWest(), zoom);
			const tileNE = Tile.fromLatLngZoom(bbox.getNorthEast(), zoom);
			
			const tiles = [];
			
			for(let y = tileNE.y; y <= tileSW.y; y++) {
				for(let x = tileSW.x; x <= tileNE.x; x++) {
					tiles.push(new Tile(x, y, zoom));
				}
			}
			
			return tiles;
		}
	}
}

module.exports = TileManager;
