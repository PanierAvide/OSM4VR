/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Action types
 */

export const SHOW_NOTIF = 'SHOW_NOTIF';
export const HIDE_NOTIF = 'HIDE_NOTIF';


/*
 * Other constants
 */

export const NotifTypes = {
	INFO: 'info',
	ALERT: 'warning',
	ERROR: 'danger',
	SUCCESS: 'success'
};


/*
 * Action creators
 */

export function showNotification(text, notiftype) {
	return { type: SHOW_NOTIF, text, notiftype };
}

export function hideNotification() {
	return { type: HIDE_NOTIF };
}
