/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cartesian3 from 'cesium/Source/Core/Cartesian3';
import CMath from 'cesium/Source/Core/Math';

/**
 * CesiumHash manages URL hash part to keep the user location in the viewer.
 * Based on {@link https://github.com/frogcat/cesium-hash/blob/master/cesium-hash.js|cesium-hash} library, edited to be an ES6 class.
 * @see {@link https://github.com/frogcat/cesium-hash}
 * @author Yuzo Matsuzawa (original)
 * @license MIT
 */
class CesiumHash {
	/**
	 * Constructor.
	 * @param {Viewer} viewer The {@link http://cesiumjs.org/Cesium/Build/Documentation/Viewer.html|Cesium viewer}
	 */
	constructor(viewer) {
		this._viewer = viewer;
		this.setPosFromHash(location.hash);
		this._lastHash = location.hash;
		
		this._viewer.camera.moveEnd.addEventListener(() => {
			location.hash = this._lastHash = this.encode();
		});
		
		window.addEventListener("hashchange", () => {
			if (this._lastHash !== location.hash) {
				this.setPosFromHash(location.hash);
			}
		});
	}
	
	/**
	 * Encodes the current position into an URL hash
	 * @return {string} The hash, for example "#43.870978/136.880634/9141049/14/-87"
	 */
	encode() {
		const camera = this._viewer.camera;
		return "#" + [
			CMath.toDegrees(camera.positionCartographic.latitude).toFixed(6),
			CMath.toDegrees(camera.positionCartographic.longitude).toFixed(6),
			Math.round(camera.positionCartographic.height),
			Math.round(CMath.toDegrees(camera.heading)),
			Math.round(CMath.toDegrees(camera.pitch))
		].join("/");
	}
	
	/**
	 * Defines the current viewer position according to given hash
	 * @param {string} hash The current hash
	 */
	setPosFromHash(hash) {
		let flag = true;
		const param = [];
		
		hash.replace(/^#/, "").split("/").forEach(function(v) {
			const a = parseFloat(v);
			if (isNaN(a)) flag = false;
			param.push(a);
		});
		
		if(flag && param.length === 5) {
			this._viewer.camera.setView({
				destination: Cartesian3.fromDegrees(param[1], param[0], param[2]),
				orientation: {
					heading: CMath.toRadians(param[3]),
					pitch: CMath.toRadians(param[4]),
					roll: 0
				}
			});
		}
	}
}

module.exports = CesiumHash;
