/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import ajax from '@fdaciuk/ajax';
import osmtogeojson from 'osmtogeojson/index';

/**
 * Overpass class allows to request {@link http://wiki.openstreetmap.org/wiki/Overpass_API|Overpass API} to retrieve data from OSM.
 * The data is retrieved, then parsed to be returned as GeoJSON.
 */
class Overpass {
	/**
	 * Constructor
	 * @param {string} server The Overpass API URL (for example http://overpass-api.de/api/)
	 * @param {Object} [options] Some options for Overpass call or parsing
	 * @param {Object} [options.polygonFeatures] Which closed ways should be considered as areas. See <a href="https://github.com/tyrasd/osmtogeojson#osmtogeojson-data-options-">osmtogeojson documentation</a> for details
	 */
	constructor(server, options) {
		this._runningQuery = false;
		this.server = server;
		this.options = options || {};
	}
	
	/**
	 * Launch a request directly to Overpass
	 * @param {LatLngBounds} bbox The bounding box of the request
	 * @param {string} req The Overpass API request, see {@link https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide|language guide} for details
	 * @param {function} success The success callback, with result GeoJSON as first parameter
	 * @param {function} fail The error callback
	 */
	rawRequest(bbox, req, success, fail) {
		if(!this._runningQuery) {
			this._runningQuery = true;
			
			const url = this.server + "interpreter";
			const query = '[out:json][timeout:25][bbox:' + bbox.getSouth() + "," + bbox.getWest() + "," + bbox.getNorth() + "," + bbox.getEast() + '];' + req;
			
			const request = ajax({ baseUrl: this.server });
			request
				.post('/interpreter', { data: query })
				.then((response, xhr) => {
					try {
						success(osmtogeojson(response, this.options));
					}
					catch(e) {
						e = e || new Error("Can't parse Overpass API response");
						fail(e);
					}
				})
				.catch((response, xhr) => {
					fail(response);
				})
				.always((response, xhr) => {
					this._runningQuery = false;
				});
		}
		//If running query, we wait
		else {
			console.log("[Overpass] Waiting for next request");
			setTimeout(
				() => { this.rawRequest(bbox, req, success, fail); },
				1000
			);
		}
	}
	
	/**
	 * Retrieves all the OSM data in the given bounds.
	 * @param {LatLngBounds} bbox The area bounds
	 * @param {function} success The success callback, with result GeoJSON as first parameter
	 * @param {function} fail The error callback
	 */
	requestAll(bbox, success, fail) {
		this.rawRequest(bbox, '(node;way;relation;);out body;>;out skel qt;', success, fail);
	}
}

module.exports = Overpass;
