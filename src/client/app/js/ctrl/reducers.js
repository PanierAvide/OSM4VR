/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import { combineReducers } from 'redux';
import {
	SHOW_NOTIF,
	HIDE_NOTIF,
	NotifTypes
} from './actions.js';
const { INFO } = NotifTypes;

function notification(state = {}, action) {
	switch (action.type) {
		case SHOW_NOTIF:
			return {
				message: action.text,
				type: action.notiftype
			};
		
		case HIDE_NOTIF:
			return {};
		
		default:
			return state;
	}
}

const osm4vrApp = combineReducers({
	notification
});

export default osm4vrApp;
