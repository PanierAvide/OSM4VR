# User interface

## URL parameters and hash

The OSM4VR web application can be called with several parameters. First, the URL will always have this structure:
```
http://your.osm4vr.instance/?<QUERY_PART>#<HASH_PART>
```

For example:
```
http://osm4vr.eu/dev/?param=yes#47.697317/-1.938388/65830/18/-55
```

### Query part

No parameters available yet.

### Hash part

The hash part is used to define the current position of the map. It follows this format:
```
#<latitude>/<longitude>/<height>/<heading>/<pitch>
```

Parameter   | Value
----------- | -----------------
latitude    | Latitude in degrees (WGS84)
longitude   | Longitude in degrees (WGS84)
height      | Height from ground in meters
heading     | Heading in degrees (North=0/360, East=90, South=180, West=270)
pitch       | Pitch in degrees (Vertical=-90, Horizontal=0)
