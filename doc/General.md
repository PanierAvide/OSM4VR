# General documentation

This document is explaining some general considerations around OSM4VR. As this framework is still a work under progress, please not that everything might not be as well documented as it could.


## What is OSM4VR ?

OSM4VR is a framework intended to render [OpenStreetMap](http://openstreetmap.org/) data into a 3D, browsable world. The idea is to provide a virtual globe which can be navigated by several people online. Then, the framework can be reused by several developers (as it is open source) to create amazing applications: the new [Second Life](https://en.wikipedia.org/wiki/Second_Life), a [Tron-like](https://en.wikipedia.org/wiki/Tron) universe, and so on.

In fact, this virtual globe will be highly configurable: you can set up your own OSM4VR instance, and choose the style of every single object. As OpenStreetMap uses a [tag system](https://wiki.openstreetmap.org/wiki/Tags), you can customise precisely the rendering of every feature of the map. For example, you can distinguish a bench which is made of wood of one made of steel, so you can render them differently.


## Why does it exist ?

*Why not ?* At start, there was the [OSM Go](https://wiki.openstreetmap.org/wiki/OSM_go) project, which offers to navigate through 3D data from OSM online, with friends. However, this project could be improved to offer new features, but needed a bit of refactoring. OSM4VR is a more generic, more configurable version of OSM Go.

Also, the purpose of a such framework is to make a fun reuse of OpenStreetMap data. A lot of volunteer contributors spend time adding many details on this map, so it's great to see it in an interactive way. We hope that this project will inspire people to create amazing applications, and also make OSM contributors add more and more details so the map will be very realistic.


## Which technologies are used ?

OSM4VR is splitted in two components: a web application and a server.

### Web application

The web application is built using modern technologies:
* HTML and CSS (of course, not *so* modern)
* [ECMAScript 6+](https://en.wikipedia.org/wiki/ECMAScript) (the modern JavaScript)
* [ReactJS](https://facebook.github.io/react/) (to create interactive user interfaces)
* [CesiumJS](http://cesiumjs.org/) (to handle the virtual Globe)
* [Webpack](http://webpack.github.io/) & [NPM](http://npmjs.com/) (for dependency management and compiling)
* [Mocha](http://mochajs.org/) (for testing features)

All these tools allows to provide modern, comprehensive source code, which works properly on web browsers. Code is splitted in classes which can be reused as is in other projects.

### Server

Still in development.


## How things works ?

OSM4VR has to manage the following tasks:
* Retrieve data from **OpenStreetMap providers** (Overpass API, vector tiles...)
* Analyze it, and **make it generic** in GeoJSON (to abstract which provider was used)
* Render the GeoJSON into **3D features** over virtual globe, according to user-defined stylesheets (in augmented MapCSS)
* Handle **multi-user interactivity** online

### Data providers

Even if OpenStreetMap is a single database, its data can be served by different ways (to avoid over-load on main server). OSM4VR will be able (soon) to handle several providers, including:
* [Overpass API](http://wiki.openstreetmap.org/wiki/Overpass_API)
* [Vector tiles](https://wiki.openstreetmap.org/wiki/Vector_tiles)

### OSM to GeoJSON

Each data provider have its output format, its particularities. To make the framework more generic, we need to abstract this retrieved data, and give to 3D renderer data in a single, standardized format. We choose to use [GeoJSON](http://geojson.org/), as it is a convenient and complete format to describe the 2D geographical data (from OpenStreetMap, but not only). So it's necessary to process data from providers to transform it in GeoJSON.

### 3D rendering of GeoJSON

As the data is standardized in GeoJSON, it's easier to write the engine which will convert this GeoJSON into 3D features, according to stylesheets. These stylesheets are written in an augmented [MapCSS](http://wiki.openstreetmap.org/wiki/MapCSS) format, which works kind of like CSS. But instead of rendering data according to DOM objects, the rules are based on the OpenStreetMap feature properties. For example:

```css
node[amenity=bench] { /* All points with amenity=bench property */
	color: #f00; /*Every kind of rendering information */
}
```

This allows to let every OSM4VR instance owner to set its own color scheme, 3D models and so on.

### Multi-user management

Still in development.
