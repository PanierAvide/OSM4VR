# Frequently asked questions

## Data / OpenStreetMap

### Why can't I see the bench/street/building next to my place ?

Maybe it's because it is not present in OpenStreetMap data. As OSM is a collaborative map of the world (it's like Wikipedia for maps), you can improve it by yourself, and add what matters to you. If you want to contribute, you can read this [beginners guide](http://learnosm.org/en/beginner/).

Note that, like Wikipedia, OpenStreetMap describes the real world. You can **only add features that exists** in the real world. Don't add objects to have a funny rendering in OSM4VR. Don't have a rollercoaster near your home so you can see it in the virtual world. If you want this rollercoaster, please call a theme park company and ask them to open one in your city. *Then*, it will be OK to add it on OSM when it will be opened to public.
