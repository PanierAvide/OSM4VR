# Read-me

[![build status](https://framagit.org/PanierAvide/OSM4VR/badges/master/build.svg)](https://framagit.org/PanierAvide/OSM4VR/commits/master)

OSM4VR (OpenStreetMap for Virtual Reality) is a project which aims to provide a 3D open world, using [OpenStreetMap](http://openstreetmap.org/) data. It is highly configurable, so you can create hyper-realistic environments, or create your own fantasy world. This 3D environment is multi-user, so you can browse this 3D map with friends and interact.

OSM4VR is inspired of [OSM Go](https://wiki.openstreetmap.org/wiki/OSM_go), which was created by [-karlos-](https://wiki.openstreetmap.org/wiki/User:-karlos-).

[![Help making this possible](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/PanierAvide/donate)


## Instances

Want to give it a try ? Here is the list of running instances:
* OSM4VR Public instance: http://osm4vr.eu/main/
* OSM4VR Development instance: http://osm4vr.eu/dev/ (may be unstable)


## Build & run

OSM4VR is split into two main components: client (web application) and server (back-end).

### Client

In order to compile the code for the client web application, you first need to install some dependencies:
* Git (repository management) - [Install instructions](https://git-scm.com/downloads)
* NPM (package management) - [Install instructions](https://docs.npmjs.com/getting-started/installing-node)

Once done, you can retrieve the source code of OSM4VR:
```
git clone https://framagit.org/PanierAvide/OSM4VR.git
cd OSM4VR/
```

Then, you can launch the dependencies retrieval, and start compiling the client web application:
```
npm install
npm run build
```

Finally, the application is ready to run. It is available in the `src/client/` folder. You can try it by two ways:
* Locally, by opening the `src/client/index.html` page in your web browser
* Distantly, by putting the content of `src/client/` folder on a web server

### Server

Still in development.


## Testing

Unit tests are available. To check if everything is OK, just run `npm run test`.


## Documentation

To know more about how OSM4VR works, you can read the documentation provided in [doc/ folder](doc/). JS code documentation is available online: [master](http://osm4vr.eu/main/doc) / [develop](http://osm4vr.eu/dev/doc).

When edited, documentation of JS code can be regenerated using `npm run doc` (and `npm run doc:lint` for syntax linting).


## Contributing

OSM4VR is open source. Contributions are welcome, and here is how you can help for this project:
* Report bugs or discuss new functionalities in the [Issues tracker](https://framagit.org/PanierAvide/OSM4VR/issues).
* Write code (and tests) and create a [merge request](https://framagit.org/PanierAvide/OSM4VR/merge_requests). As this project is using [Git Flow branching model](http://nvie.com/posts/a-successful-git-branching-model/), please create requests on __develop__ or __feature/*__ branches.


## License

Copyright 2017 OSM4VR developers

See LICENSE for complete AGPL3 license.

OSM4VR is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OSM4VR is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OSM4VR. If not, see <http://www.gnu.org/licenses/>.
