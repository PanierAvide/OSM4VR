/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

import assert from 'assert';
import FeatureFactory from '../../../src/client/app/js/model/FeatureFactory';
import Cartesian3 from 'cesium/Source/Core/Cartesian3';

/**
 * Test class for model.FeatureFactory
 */
describe('Client > Model > FeatureFactory', function() {
	describe('geojsonToEntity', function() {
		it('converts point feature into a sphere', function() {
			const ff = new FeatureFactory();
			const feature = {
				type: "Feature",
				geometry: {
					type: "Point",
					coordinates: [ -1.7, 48.6 ]
				},
				properties: {
					"amenity": "bench"
				},
				id: "node/123456"
			};
			
			const result = ff.geojsonToEntity(feature);
			assert.equal(result.id, "node/123456");
			assert.ok(result.position.equals(Cartesian3.fromDegrees(-1.7, 48.6, 0)));
			assert.ok(result.ellipsoid.radii.equals(new Cartesian3(1, 1, 1)));
		});
		
		it('converts linestring feature into a polyline', function() {
			const ff = new FeatureFactory();
			const feature = {
				type: "Feature",
				geometry: {
					type: "LineString",
					coordinates: [
						[ -1.7001, 48.6001 ],
						[ -1.7002, 48.6002 ],
						[ -1.7003, 48.6003 ]
					]
				},
				properties: {
					"highway": "steps"
				},
				id: "way/456789"
			};
			
			const result = ff.geojsonToEntity(feature);
			assert.equal(result.id, "way/456789");
			assert.ok(result.polyline.positions[0].equals(Cartesian3.fromDegrees(-1.7001, 48.6001)));
			assert.ok(result.polyline.positions[1].equals(Cartesian3.fromDegrees(-1.7002, 48.6002)));
			assert.ok(result.polyline.positions[2].equals(Cartesian3.fromDegrees(-1.7003, 48.6003)));
			assert.equal(result.polyline.width, 5);
		});
		
		it('converts simple polygon feature into a polygon', function() {
			const ff = new FeatureFactory();
			const feature = {
				type: "Feature",
				geometry: {
					type: "Polygon",
					coordinates: [ [
						[ -1.7001, 48.6001 ],
						[ -1.7001, 48.6002 ],
						[ -1.7002, 48.6002 ],
						[ -1.7002, 48.6001 ],
						[ -1.7001, 48.6001 ]
					] ]
				},
				properties: {
					"building": "house"
				},
				id: "way/789123"
			};
			
			const result = ff.geojsonToEntity(feature);
			assert.equal(result.id, "way/789123");
			assert.ok(result.polygon.hierarchy.positions[0].equals(Cartesian3.fromDegrees(-1.7001, 48.6001)));
			assert.ok(result.polygon.hierarchy.positions[1].equals(Cartesian3.fromDegrees(-1.7001, 48.6002)));
			assert.ok(result.polygon.hierarchy.positions[2].equals(Cartesian3.fromDegrees(-1.7002, 48.6002)));
			assert.ok(result.polygon.hierarchy.positions[3].equals(Cartesian3.fromDegrees(-1.7002, 48.6001)));
		});
		
		it('converts polygon with holes feature into a polygon', function() {
			const ff = new FeatureFactory();
			const feature = {
				type: "Feature",
				geometry: {
					type: "Polygon",
					coordinates: [
						[
							[ -1.7001, 48.6001 ],
							[ -1.7001, 48.6002 ],
							[ -1.7002, 48.6002 ],
							[ -1.7002, 48.6001 ],
							[ -1.7001, 48.6001 ]
						],
						[
							[ -1.70014, 48.60014 ],
							[ -1.70016, 48.60014 ],
							[ -1.70016, 48.60016 ],
							[ -1.70014, 48.60016 ],
							[ -1.70014, 48.60014 ]
						],
						[
							[ -1.70018, 48.60018 ],
							[ -1.70018, 48.60019 ],
							[ -1.70019, 48.60019 ],
							[ -1.70019, 48.60018 ],
							[ -1.70018, 48.60018 ]
						]
					]
				},
				properties: {
					"building": "house"
				},
				id: "relation/147258"
			};
			
			const result = ff.geojsonToEntity(feature);
			assert.equal(result.id, "relation/147258");
			
			assert.ok(result.polygon.hierarchy.positions[0].equals(Cartesian3.fromDegrees(-1.7001, 48.6001)));
			assert.ok(result.polygon.hierarchy.positions[1].equals(Cartesian3.fromDegrees(-1.7001, 48.6002)));
			assert.ok(result.polygon.hierarchy.positions[2].equals(Cartesian3.fromDegrees(-1.7002, 48.6002)));
			assert.ok(result.polygon.hierarchy.positions[3].equals(Cartesian3.fromDegrees(-1.7002, 48.6001)));
			
			assert.ok(result.polygon.hierarchy.holes[0].positions[0].equals(Cartesian3.fromDegrees(-1.70014, 48.60014)));
			assert.ok(result.polygon.hierarchy.holes[0].positions[1].equals(Cartesian3.fromDegrees(-1.70016, 48.60014)));
			assert.ok(result.polygon.hierarchy.holes[0].positions[2].equals(Cartesian3.fromDegrees(-1.70016, 48.60016)));
			assert.ok(result.polygon.hierarchy.holes[0].positions[3].equals(Cartesian3.fromDegrees(-1.70014, 48.60016)));
			
			assert.ok(result.polygon.hierarchy.holes[1].positions[0].equals(Cartesian3.fromDegrees(-1.70018, 48.60018)));
			assert.ok(result.polygon.hierarchy.holes[1].positions[1].equals(Cartesian3.fromDegrees(-1.70018, 48.60019)));
			assert.ok(result.polygon.hierarchy.holes[1].positions[2].equals(Cartesian3.fromDegrees(-1.70019, 48.60019)));
			assert.ok(result.polygon.hierarchy.holes[1].positions[3].equals(Cartesian3.fromDegrees(-1.70019, 48.60018)));
		});
	});
});
