/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Test file for model.LatLng
 */

import assert from 'assert';
import LatLng from '../../../src/client/app/js/model/LatLng';

describe('Client > Model > LatLng', function() {
	describe('constructor', function() {
		it('sets lat and lng', function() {
			const a = new LatLng(25, 74);
			assert.equal(a.lat, 25);
			assert.equal(a.lng, 74);
			
			const b = new LatLng(-25, -74);
			assert.equal(b.lat, -25);
			assert.equal(b.lng, -74);
		});
		
		it('throws an error if invalid lat or lng', function() {
			assert.throws(
				() => { const a = new LatLng(NaN, NaN); },
				Error,
				"Invalid LatLng object: (NaN, NaN)"
			);
		});
		
		it('does not set altitude if undefined', function() {
			const a = new LatLng(25, 74);
			assert.equal(typeof a.alt, 'undefined');
		});
		
		it('sets altitude', function() {
			const a = new LatLng(25, 74, 50);
			assert.equal(a.alt, 50);
			
			const b = new LatLng(-25, -74, -50);
			assert.equal(b.alt, -50);
		});
	});
	
	describe('distanceTo', function() {
		it('calculates distance', function() {
			const p1 = new LatLng(36.12, -86.67);
			const p2 = new LatLng(33.94, -118.40);
			assert.ok(p1.distanceTo(p2) >= 2886444.43);
			assert.ok(p1.distanceTo(p2) <= 2886444.45);
		});
	});
});
