/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Test class for model.Tile
 */

import assert from 'assert';
import LatLng from '../../../src/client/app/js/model/LatLng';
import Tile from '../../../src/client/app/js/model/Tile';

describe('Client > Model > Tile', function() {
	describe('constructor', function() {
		it('sets x, y and z', function() {
			const t1 = new Tile(1, 2, 3);
			assert.equal(t1.x, 1);
			assert.equal(t1.y, 2);
			assert.equal(t1.z, 3);
		});
	});
	
	describe('fromLatLngZoom', function() {
		it('creates a tile according to latlng + zoom', function() {
			const t1 = Tile.fromLatLngZoom(new LatLng(48.10449, -1.67411), 17);
			assert.equal(t1.x, 64926);
			assert.equal(t1.y, 45505);
			assert.equal(t1.z, 17);
		});
		
		it('handles extreme longitude values', function() {
			const t1 = Tile.fromLatLngZoom(new LatLng(65.80327, 179.99981), 19);
			assert.equal(t1.x, 524287);
			assert.equal(t1.y, 133630);
			assert.equal(t1.z, 19);
		});
	});
	
	describe('getId', function() {
		it('returns ID', function() {
			const t1 = new Tile(1, 2, 3);
			assert.equal(t1.getId(), "3/1/2");
		});
	});
	
	describe('getBounds', function() {
		it('returns bounds', function() {
			const t1 = new Tile(64926, 45505, 17);
			const b1 = t1.getBounds();
			assert.ok(b1.getSouthWest().distanceTo(new LatLng(48.10375, -1.67531)) <= 10);
			assert.ok(b1.getNorthEast().distanceTo(new LatLng(48.10558, -1.67269)) <= 10);
		});
	});
});
