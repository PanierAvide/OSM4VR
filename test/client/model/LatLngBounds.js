/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Test file for model.LatLngBounds
 */

import assert from 'assert';
import LatLng from '../../../src/client/app/js/model/LatLng';
import LatLngBounds from '../../../src/client/app/js/model/LatLngBounds';
var that = {};

describe('Client > Model > LatLngBounds', function() {
	beforeEach(function () {
		that = {};
		that.a = new LatLngBounds(
			new LatLng(14, 12),
			new LatLng(30, 40));
		that.c = new LatLngBounds();
	});

	describe('constructor', function() {
		it('instantiates either passing two latlngs or an array of latlngs', function() {
			const b = new LatLngBounds([
				new LatLng(14, 12),
				new LatLng(30, 40)
			]);
			assert.ok(b.equals(that.a));
			assert.ok(b.getNorthWest().equals(new LatLng(30, 12)));
		});

		it('returns an empty bounds when not argument is given', function() {
			const bounds = new LatLngBounds();
			assert.ok(bounds instanceof LatLngBounds);
		});
	});

	describe('extend', function() {
		it('extends the bounds by a given point', function() {
			that.a.extend(new LatLng(20, 50));
			assert.ok(that.a.getNorthEast().equals(new LatLng(30, 50)));
		});

		it('extends the bounds by undefined', function() {
			assert.ok(that.a.extend().equals(that.a));
		});

		it('extends the bounds by raw object', function() {
			that.a.extend({lat: 20, lng: 50});
			assert.ok(that.a.getNorthEast().equals(new LatLng(30, 50)));
		});

		it('extend the bounds by an empty bounds object', function() {
			assert.ok(that.a.extend(new LatLngBounds()).equals(that.a));
		});
	});

	describe('getCenter', function() {
		it('returns the bounds center', function() {
			assert.ok(that.a.getCenter().equals(new LatLng(22, 26)));
		});
	});

	describe('pad', function() {
		it('pads the bounds by a given ratio', function() {
			const b = that.a.pad(0.5);

			assert.ok(b.equals(new LatLngBounds([[6, -2], [38, 54]])));
		});
	});

	describe('equals', function() {
		it('returns true if bounds equal', function() {
			assert.ok(that.a.equals([[14, 12], [30, 40]]));
			assert.ok(!that.a.equals([[14, 13], [30, 40]]));
			assert.ok(!that.a.equals(null));
		});
	});

	describe('isValid', function() {
		it('returns true if properly set up', function() {
			assert.ok(that.a.isValid());
		});
		it('returns false if is invalid', function() {
			assert.ok(!that.c.isValid());
		});
		it('returns true if extended', function() {
			that.c.extend([0, 0]);
			assert.ok(that.c.isValid());
		});
	});

	describe('getWest/South/East/North', function() {
		it('returns a proper bbox west value', function() {
			assert.equal(that.a.getWest(), 12);
		});

		it('returns a proper bbox south value', function() {
			assert.equal(that.a.getSouth(), 14);
		});

		it('returns a proper bbox east value', function() {
			assert.equal(that.a.getEast(), 40);
		});

		it('returns a proper bbox north value', function() {
			assert.equal(that.a.getNorth(), 30);
		});
	});

	describe('toBBoxString', function() {
		it('returns a proper left,bottom,right,top bbox', function() {
			assert.equal(that.a.toBBoxString(), "12,14,40,30");
		});
	});

	describe('toNorthWest/SouthEast', function() {
		it('returns a proper north-west LatLng', function() {
			assert.ok(that.a.getNorthWest().equals(new LatLng(that.a.getNorth(), that.a.getWest())));
		});

		it('returns a proper south-east LatLng', function() {
			assert.ok(that.a.getSouthEast().equals(new LatLng(that.a.getSouth(), that.a.getEast())));
		});
	});

	describe('contains', function() {
		it('returns true if contains latlng point as array', function() {
			assert.ok(that.a.contains([16, 20]));
			assert.ok(!new LatLngBounds(that.a).contains([5, 20]));
		});

		it('returns true if contains latlng point as {lat:, lng:} object', function() {
			assert.ok(that.a.contains({lat: 16, lng: 20}));
			assert.ok(!new LatLngBounds(that.a).contains({lat: 5, lng: 20}));
		});

		it('returns true if contains latlng point as LatLng instance', function() {
			assert.ok(that.a.contains(new LatLng([16, 20])));
			assert.ok(!new LatLngBounds(that.a).contains(new LatLng([5, 20])));
		});

		it('returns true if contains bounds', function() {
			assert.ok(that.a.contains([[16, 20], [20, 40]]));
			assert.ok(!that.a.contains([[16, 50], [8, 40]]));
		});
	});

	describe('intersects', function() {
		it('returns true if intersects the given bounds', function() {
			assert.ok(that.a.intersects([[16, 20], [50, 60]]));
			assert.ok(!that.a.contains([[40, 50], [50, 60]]));
		});

		it('returns true if just touches the boundary of the given bounds', function() {
			assert.ok(that.a.intersects([[25, 40], [55, 50]]));
		});
	});

	describe('overlaps', function() {
		it('returns true if overlaps the given bounds', function() {
			assert.ok(that.a.overlaps([[16, 20], [50, 60]]));
		});
		it('returns false if just touches the boundary of the given bounds', function() {
			assert.ok(!that.a.overlaps([[25, 40], [55, 50]]));
		});
	});
});
