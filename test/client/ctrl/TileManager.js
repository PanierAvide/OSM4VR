/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Test file for ctrl.TileManager
 */

import assert from 'assert';
import LatLng from '../../../src/client/app/js/model/LatLng';
import LatLngBounds from '../../../src/client/app/js/model/LatLngBounds';
import Tile from '../../../src/client/app/js/model/Tile';
import TileManager from '../../../src/client/app/js/ctrl/TileManager';

describe('Client > Ctrl > TileManager', function() {
	describe('constructor', function() {
		it('creates Overpass provider instance', function() {
			const t1 = new TileManager();
			assert.ok(t1._overpass.server != undefined);
		});
	});
	
	describe('bbox2tiles', function() {
		it('gives the list of tiles covering the bounding box', function() {
			const t1 = new TileManager();
			const bbox = new LatLngBounds(new LatLng(48.10512, -1.673), new LatLng(48.10673, -1.67046));
			
			const result = t1.bbox2tiles(bbox, 17);
			
			assert.equal(result.length, 4);
			
			assert.equal(result[0].z, 17);
			assert.equal(result[0].x, 64926);
			assert.equal(result[0].y, 45504);
			
			assert.equal(result[1].z, 17);
			assert.equal(result[1].x, 64927);
			assert.equal(result[1].y, 45504);
			
			assert.equal(result[2].z, 17);
			assert.equal(result[2].x, 64926);
			assert.equal(result[2].y, 45505);
			
			assert.equal(result[3].z, 17);
			assert.equal(result[3].x, 64927);
			assert.equal(result[3].y, 45505);
		});
		
		it('handles > 180 longitude', function() {
			const t1 = new TileManager();
			const bbox = new LatLngBounds(new LatLng(65.80327, 179.99981), new LatLng(65.80341, 180.00020));
			
			const result = t1.bbox2tiles(bbox, 19);
			
			assert.equal(result.length, 4);
			assert.equal(result[0].getId(), "19/524287/133629");
			assert.equal(result[1].getId(), "19/524287/133630");
			assert.equal(result[2].getId(), "19/0/133629");
			assert.equal(result[3].getId(), "19/0/133630");
		});
		
		it('handles < -180 longitude', function() {
			const t1 = new TileManager();
			const bbox = new LatLngBounds(new LatLng(65.80327, -180.00019), new LatLng(65.80341, -179.9998));
			
			const result = t1.bbox2tiles(bbox, 19);
			
			assert.equal(result.length, 4);
			assert.equal(result[0].getId(), "19/524287/133629");
			assert.equal(result[1].getId(), "19/524287/133630");
			assert.equal(result[2].getId(), "19/0/133629");
			assert.equal(result[3].getId(), "19/0/133630");
		});
	});
	
	describe('getTiles', function() {
		it('returns appropriate tiles data', function() {
			const bbox = new LatLngBounds(new LatLng(48.11177, -2.12648), new LatLng(48.11184, -2.12639));
			const tm1 = new TileManager();
			
			return tm1.getTiles(bbox, 19)
				.then((tiles) => {
					assert.equal(Object.keys(tiles).length, 1);
					assert.ok(tiles["19/259047/182006"] !== undefined);
					
					const tileData = tiles["19/259047/182006"];
					assert.equal(tileData.tile.x, 259047);
					assert.equal(tileData.tile.y, 182006);
					assert.equal(tileData.tile.z, 19);
					
					assert.ok(tileData.data !== undefined);
					const geojson = tileData.data;
					
					let foundAddress = 0;
					for(const f of geojson.features) {
						if(f.properties["addr:housenumber"] === "1") {
							foundAddress++;
						}
					}
					
					assert.equal(foundAddress, 1);
				})
				.catch((error) => {
					assert.fail(error);
				});
		}).timeout(30000);
	});
	
	describe('dropTiles', function() {
		it('removes tiles too far from given position', function() {
			const t1 = new Tile(50,50,10);
			const t2 = new Tile(49,49,10);
			const t3 = new Tile(51,50,10);
			const t4 = new Tile(500,50,10);
			const t5 = new Tile(49,1200,9);
			
			const tm1 = new TileManager();
			tm1._loadedTiles = {
				"10/50/50": { tile: t1 },
				"10/49/49": { tile: t2 },
				"10/51/50": { tile: t3 },
				"10/500/50": { tile: t4 },
				"9/49/1200": { tile: t5 }
			};
			
			const droppedTiles = tm1.dropTiles(t1.getBounds().getCenter(), 10);
			
			assert.equal(tm1._loadedTiles["10/50/50"].tile, t1);
			assert.equal(tm1._loadedTiles["10/49/49"].tile, t2);
			assert.equal(tm1._loadedTiles["10/51/50"].tile, t3);
			assert.ok(tm1._loadedTiles["10/500/50"] === undefined);
			assert.equal(tm1._loadedTiles["9/49/1200"].tile, t5);
			assert.equal(droppedTiles["10/500/50"].tile, t4);
		});
	});
});
