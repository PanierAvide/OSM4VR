/*
 * This file is part of OSM4VR.
 * 
 * OSM4VR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OSM4VR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OSM4VR.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Test file for ctrl.provider.Overpass
 */

import assert from 'assert';
import Overpass from '../../../../src/client/app/js/ctrl/provider/Overpass';
import LatLng from '../../../../src/client/app/js/model/LatLng';
import LatLngBounds from '../../../../src/client/app/js/model/LatLngBounds';
import CONFIG from '../../../../src/client/config/config.json';

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var OAPI = CONFIG.data_providers.overpass;

describe.skip('Client > Ctrl > Provider > Overpass', function() {
	describe('constructor', function() {
		it('sets server and options', function() {
			const o1 = new Overpass(
							OAPI,
							{
								polygonFeatures: { randomVal: true }
							}
						);
			
			assert.equal(o1.server, OAPI);
			assert.equal(o1.options.polygonFeatures.randomVal, true);
		});
	});
	
	describe('rawRequest', function() {
		it('gets response from OAPI', function(done) {
			const o1 = new Overpass(OAPI);
			const success = (geojson) => {
				assert.equal(geojson.features[0].properties["addr:housenumber"], 1);
				done();
			};
			const fail = (error) => {
				assert.fail(error);
				done();
			};
			o1.rawRequest(new LatLngBounds(new LatLng(48.13004842632867,-1.6767145693302155), new LatLng(48.13022297240938,-1.6764409840106964)), '(node["addr:housenumber"]);out body;', success, fail);
		}).timeout(15000);
		
		it('doesn\'t run several requests at the same time', function(done) {
			const o1 = new Overpass(OAPI);
			let successes = 0;
			const success = (geojson) => {
				assert.ok(geojson.features.length > 0);
				successes++;
				if(successes == 3) {
					done();
				}
			};
			const fail = (error) => {
				assert.fail(error);
				done();
			};
			
			o1.rawRequest(new LatLngBounds(new LatLng(48.13004842632867,-1.6767145693302155), new LatLng(48.13022297240938,-1.6764409840106964)), '(node["addr:housenumber"]);out body;', success, fail);
			o1.rawRequest(new LatLngBounds(new LatLng(48.13004842632867,-1.6767145693302155), new LatLng(48.13022297240938,-1.6764409840106964)), '(way["building"]);out body;>;out skel qt;', success, fail);
			o1.rawRequest(new LatLngBounds(new LatLng(48.13004842632,-1.6767145693302), new LatLng(48.13022297240,-1.6764409840106)), '(node["addr:housenumber"]);out body;', success, fail);
		}).timeout(60000);
		
		it('calls fail function if needed', function(done) {
			const o1 = new Overpass(OAPI);
			const success = (geojson) => {
				assert.fail("Should not have succeeded");
				done();
			};
			const fail = (error) => {
				assert.ok(true);
				done();
			};
			o1.rawRequest(new LatLngBounds(new LatLng(48.13004842632867,-1.6767145693302155), new LatLng(48.13022297240938,-1.6764409840106964)), 'THIS IS NOT A PROPER REQUEST;', success, fail);
		}).timeout(10000);
	});
	
	describe('requestAll', function() {
		it('retrieves everything from OSM', function(done) {
			const o1 = new Overpass(OAPI);
			const success = (geojson) => {
				assert.ok(geojson.features.length > 0);
				done();
			};
			const fail = (error) => {
				assert.fail(error);
				done();
			};
			o1.requestAll(new LatLngBounds(new LatLng(48.13004842632867,-1.6767145693302155), new LatLng(48.13022297240938,-1.6764409840106964)), success, fail);
		}).timeout(30000);
	});
});
