const webpack = require('webpack');
const path = require('path');

const BUILD_DIR = path.resolve(__dirname, 'src/client/public');
const APP_DIR = path.resolve(__dirname, 'src/client/app');
const TEST_DIR = path.resolve(__dirname, 'test/client');

const config = {
	entry: APP_DIR + '/index.js',
	devtool: 'source-map',
	target: 'web',
	output: {
		path: BUILD_DIR,
		filename: 'osm4vr.js',
		sourcePrefix : '',
		publicPath: 'public/'
	},
	resolve: {
		"alias": {
			"react": "preact-compat",
			"react-dom": "preact-compat"
		}
	},
	module : {
		unknownContextCritical : false,
		unknownContextRegExp: /^.\/.*$/,
		loaders: [
			{
				test: /\.jsx?$/,
				include: [ APP_DIR, TEST_DIR ],
				loader: 'babel-loader'
			},
			{
				test: /\.json$/,
				loader: 'compact-json-loader'
			},
			{
				test: /\.css$/,
				loader: "style-loader!css-loader"
			},
			{
				test: /\.png$/,
				loader: "url-loader?limit=100000"
			},
			{
				test: /\.jpg$/,
				loader: "file-loader"
			},
			{
				test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'url-loader?limit=10000&mimetype=application/font-woff'
			},
			{
				test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
			},
			{
				test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'file-loader'
			},
			{
				test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: JSON.stringify("production")
			}
		})
	]
};

if(process.env.NODE_ENV === 'test') {
	config.target = 'node';
}

if(process.env.NODE_ENV === 'production') {
	config.plugins = config.plugins.concat([
		new webpack.optimize.UglifyJsPlugin({
			comments: false,
			mangle: true,
			compress: {
				unused: true,
				dead_code: true,
				warnings: false,
				drop_debugger: true,
				conditionals: true,
				evaluate: true,
				drop_console: true,
				sequences: true,
				booleans: true
			}
		}),
		new webpack.optimize.AggressiveMergingPlugin()
	]);
}

module.exports = config;
